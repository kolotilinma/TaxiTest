//
//  SMSCodeViewController.swift
//  TaxiTest
//
//  Created by Mikhail Kolotilin on 24.04.2021.
//

import UIKit

class SMSCodeViewController: UIViewController {
    
    //MARK: - Properties
    var phone: String?
    var code: String? {
        didSet {
            hideAndShowDoneButton(with: code)
        }
    }
    var token: String? {
        didSet {
            guard let token = token else { return }
            getUserInfo(with: token)
        }
    }
    let alert = AlertService.shared
    private var timer: Timer?
    private var timeCounter = 15
    
    fileprivate let topLabel: UILabel = {
        let label = UILabel()
        label.text = "Введите код из СМС"
        label.font = UIFont(name: "Roboto-Bold", size: 24)
        return label
    }()
    
    fileprivate let bottomLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Roboto-Regular", size: 14)
        label.textColor = #colorLiteral(red: 0.592, green: 0.592, blue: 0.592, alpha: 1)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    fileprivate let smsCodeField: OneTimeCodeTextField = {
        let field = OneTimeCodeTextField()
        field.defaultCharacter = " "
        field.configure()
        return field
    }()
    
    private lazy var loginButton: UIButton = {
        let button = UIButton(title: "Готово", titleColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), backgroundColor: #colorLiteral(red: 0.016, green: 0.482, blue: 0.973, alpha: 1))
        button.isHidden = true
        button.addTarget(self, action: #selector(handleSentSMS), for: .touchUpInside)
        return button
    }()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        setupElements()
//        hideKeyboardWhenTappedAround()
        startTimer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        smsCodeField.becomeFirstResponder()
    }
    
    //MARK: - Actions
    @objc
    private func handleSentSMS() {
        NetworkService.getCredentials(phone: phone, smsCode: code) { (result) in
            switch result {
            case .success(let token):
                self.token = token
            case .failure(let err):
                DispatchQueue.main.async {
                    let alert = self.alert.showMessage(title: "Ошибка", with: err.myDescription)
                    self.present(alert, animated: true)
                    print(err.localizedDescription)
                }
            }
        }
        
        
//        let alert = self.alert.showMessage(title: "Succses", with: code ?? "")
//        self.present(alert, animated: true)
        
    }
    
    @objc
    private func textDidChange() {
        guard let code = smsCodeField.text else { return }
        self.code = code
    }
    
    private func getUserInfo(with token: String) {
        NetworkService.getInfoUser(token: token) { (result) in
            switch result {
            case .success(let user):
                print("Succses", user.email)
                DispatchQueue.main.async {
                    let alert = self.alert.showMessage(
                        title: "Приветствую тебя \(user.name)!",
                        with: "У тебя уже заполнены:\n status: \(user.status)\n id: \(user.id)\n phone_number: \(user.phoneNumber)\n name: \(user.name)\n email: \(user.email)\n sex : \(user.sex ?? "")\n birth_day: \(user.birthDay ?? "")\n city \(user.city)\n rating: \(user.rating)\n active_order: \(user.activeOrder ?? "")\n organization_id: \(user.organizationId ?? "") \n need_registration: \(user.needRegistration)")
                   
                    self.present(alert, animated: true)
                }
            case .failure(let err):
                DispatchQueue.main.async {
                    let alert = self.alert.showMessage(title: "Ошибка", with: err.myDescription)
                    self.present(alert, animated: true)
                    print(err.localizedDescription)
                }
            }
        }
    }
    
    // MARK: - Helper
    fileprivate func setupElements() {
        view.addSubview(topLabel)
        topLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor,
                        paddingTop: 108, paddingLeft: 38)
        
        view.addSubview(smsCodeField)
        smsCodeField.centerX(inView: view)
        smsCodeField.anchor(top: topLabel.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor,
                            paddingTop: 24, paddingLeft: 38, paddingRight: 38, height: 64)
        smsCodeField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        
        smsCodeField.didEnterLastDigit = { [weak self] code in
            self?.code = code
        }
        view.addSubview(bottomLabel)
        bottomLabel.centerX(inView: view, topAnchor: smsCodeField.bottomAnchor, paddingTop: 60)
        bottomLabel.anchor(left: view.leftAnchor, right: view.rightAnchor,
                           paddingLeft: 31 , paddingRight: 31)
        
        view.addSubview(loginButton)
        loginButton.anchor(left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor,
                           right: view.rightAnchor, paddingLeft: 31, paddingBottom: 280, paddingRight: 31, height: 57)
//        loginButton.anchor(top: smsCodeField.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor,
//                           paddingTop: 162, paddingLeft: 31 , paddingRight: 31, height: 57)
        
    }
    
    fileprivate func startTimer() {
        timeCounter = 15
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.changeTitle),
                                     userInfo: nil, repeats: true)
    }
    
    @objc
    func changeTitle() {
        if timeCounter != 0 {
            bottomLabel.text = "Повторно отправить СМС с кодом можно будет через \(timeCounter) секунд"
            timeCounter -= 1
        } else {
            cancelTimer()
        }
    }
    
    @objc
    func resendSMS() {
        bottomLabel.text = ""
        bottomLabel.textColor = #colorLiteral(red: 0.592, green: 0.592, blue: 0.592, alpha: 1)
        NetworkService.requestSMSCode(phone: phone) { (result) in
            switch result {
            case .success(_):
                print("Success")
                DispatchQueue.main.async {
                    self.startTimer()
                }
            case .failure(let err):
                print(err.localizedDescription)
            }
        }
    }
    
    fileprivate func cancelTimer() {
        timer?.invalidate()
        timer = nil
        bottomLabel.text = "Отправить СМС с кодом повторно"
        bottomLabel.textColor = #colorLiteral(red: 0.016, green: 0.482, blue: 0.973, alpha: 1)
        bottomLabel.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(resendSMS))
        bottomLabel.addGestureRecognizer(tapGesture)
    }
    
    fileprivate func hideAndShowDoneButton(with code: String?) {
        guard let code = code else { return }
        if code.count == 4 {
            loginButton.isHidden = false
        } else {
            loginButton.isHidden = true
        }
    }
    
    
}
