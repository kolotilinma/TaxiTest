//
//  PhoneViewController.swift
//  TaxiTest
//
//  Created by Mikhail Kolotilin on 22.04.2021.
//

import UIKit

class PhoneViewController: UIViewController {

    //MARK: - Properties
    private let logoImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "xc")
        iv.contentMode = .scaleAspectFill
        iv.layer.cornerRadius = iv.frame.height / 2
        iv.clipsToBounds = true
        return iv
    }()
    
    private let offerLabel: UILabel = {
        let label = UILabel()
        label.text = "Ознакомьтесь с договором-оферты. Регистрируясь или\nавторизуясь, вы принимаете его условия"
        label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        label.font = UIFont(name: "Roboto-Light", size: 10)
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private lazy var loginButton: UIButton = {
        let button = UIButton(title: "Продолжить")
        button.addTarget(self, action: #selector(handleSentSMS), for: .touchUpInside)
        return button
    }()
    
    private let phoneTextField: PhoneNumberTextField = {
        let tf = PhoneNumberTextField()
        tf.maskString = "000 000 00-00"
        return tf
    }()
    
    let alert = AlertService.shared


    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 0, green: 0.5714389682, blue: 0.9794111848, alpha: 1)
        setupElements()
        hideKeyboardWhenTappedAround()
    }
    
    //MARK: - Actions
    @objc private func handleSentSMS() {
        requestSMSCode(phone: phoneTextField.phoneNumberText)
    }

    // MARK: - Helper
    fileprivate func setupElements() {
        view.addSubview(logoImageView)
        logoImageView.setDimensions(height: 172, width: 172)
        logoImageView.centerX(inView: view,
                              topAnchor: view.safeAreaLayoutGuide.topAnchor,
                              paddingTop: 129)
        
        view.addSubview(offerLabel)
        offerLabel.centerX(inView: view)
        offerLabel.anchor(left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor,
                          right: view.rightAnchor, paddingBottom: 15)
        
        view.addSubview(loginButton)
        loginButton.anchor(left: view.leftAnchor, bottom: offerLabel.topAnchor, right: view.rightAnchor,
                           paddingLeft: 31, paddingBottom: 15, paddingRight: 31, height: 57)
        
        view.addSubview(phoneTextField)
        phoneTextField.anchor(left: view.leftAnchor, bottom: loginButton.topAnchor, right: view.rightAnchor,
                              paddingLeft: 31, paddingBottom: 49, paddingRight: 31, height: 50)
        
    }
        
    
    // MARK: - API
    fileprivate func requestSMSCode(phone: String?) {
        if phoneTextField.phoneNumberText?.count == 11 {
            NetworkService.requestSMSCode(phone: phone) { (result) in
                switch result {
                case .success(_):
                    DispatchQueue.main.async {
                        let smsVC = SMSCodeViewController()
                        smsVC.modalPresentationStyle = .fullScreen
                        smsVC.phone = phone
                        self.present(smsVC, animated: true, completion: nil)
                    }
                case .failure(let err):
                    DispatchQueue.main.async {
                        let alert = self.alert.showMessage(title: "Ошибка", with: err.myDescription)
                        self.present(alert, animated: true)
                        print(err.localizedDescription)
                    }
                }
            }
        } else {
            print("Некоректно введен номер телефона")
            let alert = self.alert.showMessage(title: "Ошибка", with: "Некоректно введен номер телефона")
            self.present(alert, animated: true)
        }
    }
    
    
}
