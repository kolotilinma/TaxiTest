//
//  NetworkService.swift
//  TaxiTest
//
//  Created by Mikhail Kolotilin on 23.04.2021.
//

import Foundation

class NetworkService {
    
    static private let basePath = "https://dev.pulttaxi.ru/api/"
    static let defaultSession = URLSession(configuration: .default)
    static var dataTask: URLSessionDataTask?
    
    class func requestSMSCode(phone: String?, completion: @escaping (_ result: (Result<String, NetworkManagerError>)) -> Void) {
        guard let phone = phone else { return }
        guard var urlComponents = URLComponents(string: basePath + "requestSMSCodeClient") else { return }
        urlComponents.query = "phone_number=\(phone)"
        guard let url = urlComponents.url else { return }
        dataTask?.cancel()
        dataTask = defaultSession.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                completion(.failure(.UnknownError))
            }
            if let httpResponse = response as? HTTPURLResponse {
                switch httpResponse.statusCode {
                case 200..<300:
                    completion(.success("Success"))
                case 400..<500:
                    print("Request error, statusCode:", httpResponse.statusCode)
                    completion(.failure(.RequestError))
                    return
                case 500..<600:
                    print("Server error, statusCode:", httpResponse.statusCode)
                    completion(.failure(.ServerError))
                    return
                case let otherCode:
                    print("Other code: \(otherCode)")
                    completion(.failure(.UnknownError))
                    return
                }
            }
        })
        dataTask?.resume()
    }

    class func getCredentials(phone: String?, smsCode: String?, completion: @escaping (_ result: (Result<String, NetworkManagerError>)) -> Void ) {
        
        guard let phone = phone, let smsCode = smsCode else { return }
        guard var urlComponents = URLComponents(string: basePath + "authenticateClients") else { return }
        urlComponents.query = "phone_number=\(phone)&password=\(smsCode)"
        guard let url = urlComponents.url else { return }
        dataTask?.cancel()
        
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData)
        request.httpMethod = "POST"
        dataTask = defaultSession.dataTask(with: request) { (data, response, error) in
            if error != nil {
                completion(.failure(.UnknownError))
            }
            if let httpResponse = response as? HTTPURLResponse {
                switch httpResponse.statusCode {
                case 200..<300:
                    print("Success")
                case 400..<500:
                    print("Request error, statusCode:", httpResponse.statusCode)
                    completion(.failure(.RequestError))
                    return
                case 500..<600:
                    print("Server error, statusCode:", httpResponse.statusCode)
                    completion(.failure(.ServerError))
                    return
                case let otherCode:
                    print("Other code: \(otherCode)")
                    completion(.failure(.UnknownError))
                    return
                }
            }
            if let data = data {
                if let jsonDictionary = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: String] {
                    if jsonDictionary["error"] == "invalid_credentials" {
                        completion(.failure(.SMSError))
                        return
                    }
                }
                do {
                    let tokenModel = try JSONDecoder().decode(Token.self, from: data)
                    completion(.success(tokenModel.token))
                } catch {
                    print(error.localizedDescription)
                    completion(.failure(.DataDecodingError))
                }
            }
        }
        dataTask?.resume()
    }

    class func getInfoUser(token: String?, completion: @escaping (_ result: (Result<UserData, NetworkManagerError>)) -> Void ) {
        guard let token = token else { return }
        guard var urlComponents = URLComponents(string: basePath + "client/getInfo") else { return }
        urlComponents.query = "token=\(token)"
        guard let url = urlComponents.url else { return }
        print(url)
        dataTask?.cancel()
        dataTask = defaultSession.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                completion(.failure(.UnknownError))
            }
            if let httpResponse = response as? HTTPURLResponse {
                    switch httpResponse.statusCode {
                    case 200..<300:
                        print("Success")
                    case 400..<500:
                        print("Request error, statusCode:", httpResponse.statusCode)
                        completion(.failure(.RequestError))
                        return
                    case 500..<600:
                        print("Server error, statusCode:", httpResponse.statusCode)
                        completion(.failure(.ServerError))
                        return
                    case let otherCode:
                        print("Other code: \(otherCode)")
                        completion(.failure(.UnknownError))
                        return
                    }
                }
            if let data = data {
                do {
                    let userModel = try JSONDecoder().decode(UserData.self, from: data)
                    print(userModel)
                    completion(.success(userModel))
                } catch {
                    print(error.localizedDescription)
                    completion(.failure(.DataDecodingError))
                }
            }
            
        })
        dataTask?.resume()
    }

}



enum NetworkManagerError: String, Error{
    case NoData
    case ServerError
    case Forbidden
    case DataDecodingError
    case UnknownError
    case RequestError
    case SMSError
    case NoInternetConnection
    
    var myDescription: String {
        switch self {
        case .NoData:
            return "Ошибка сервера"
        case .ServerError:
            return "Ошибка сервера"
        case .Forbidden:
            return "Ошибка запроса"
        case .DataDecodingError:
            return "Ошибка данных"
        case .UnknownError:
            return "Неизвестная ошибка"
        case .RequestError:
            return "Ошибка запроса"
        case .SMSError:
            return "Не корректный СМС код :("
        case .NoInternetConnection:
            return "Отсутствует интернет соединение"
        }
    }
}
