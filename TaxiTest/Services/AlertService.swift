//
//  AlertService.swift
//  TaxiTest
//
//  Created by Mikhail Kolotilin on 24.04.2021.
//

import UIKit

class AlertService {
    
    private init() {}
    static let shared = AlertService()
    
    func showMessage(title: String,with message: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel)
        alert.addAction(action)
        return alert
    }
}
