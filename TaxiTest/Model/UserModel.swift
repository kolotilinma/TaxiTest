//
//  UserModel.swift
//  TaxiTest
//
//  Created by Mikhail Kolotilin on 24.04.2021.
//

import Foundation

struct UserData: Codable {
    let status: Int
    let id: Int
    let phoneNumber: String
    let name: String
    let email: String
    let sex: String?
    let birthDay: String?
    let city: String
    let rating: String
    let activeOrder: String?
    let organizationId: String?
    let needRegistration: Bool
    
    enum CodingKeys: String, CodingKey {
        case status
        case id
        case phoneNumber = "phone_number"
        case name
        case email
        case sex
        case birthDay = "birth_day"
        case city
        case rating
        case activeOrder = "active_order"
        case organizationId = "organization_id"
        case needRegistration = "need_registration"
    }
    
}

struct Token: Codable {
    let token: String
}
