//
//  PhoneNumberTextField.swift
//  TaxiTest
//
//  Created by Mikhail Kolotilin on 23.04.2021.
//

import UIKit

class PhoneNumberTextField: UITextField {

    //MARK: - Properties
    override var text: String? {
        didSet {
            guard let text = text else { return }
            if text.last == " " || text.last == "-" {
                self.text = String(text.dropLast())
            }
            placeholderLabel.isHidden = !text.isEmpty
        }
    }
    public private(set) var stringMask: StringMask?
    fileprivate weak var realDelegate: UITextFieldDelegate?
    public var unmaskedText: String? {
        get {
            return self.stringMask?.unmask(string: self.text) ?? self.text
        }
    }
    public var phoneNumberText: String? {
        get {
            return String("7" + (unmaskedText ?? ""))
        }
    }
    open var maskString: String? {
        didSet {
            guard let maskString = self.maskString else { return }
            self.stringMask = StringMask(mask: maskString)
        }
    }
    
    let placeholderLabel: UILabel = {
        let label = UILabel()
        label.font  = UIFont(name: "Roboto-Light", size: 12)
        label.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        label.text = "Введите номер вашего телефона"
        label.textAlignment = .center
        return label
    }()

    override weak open var delegate: UITextFieldDelegate? {
        get {
            return self.realDelegate
        }
        set (newValue) {
            self.realDelegate = newValue
            super.delegate = self
        }
    }
    
    //MARK: - View LifeCycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
        
        addSubview(placeholderLabel)
        placeholderLabel.centerY(inView: self, paddingLeft: 20)
        placeholderLabel.anchor(left: leftAnchor, right: rightAnchor)
        
        let devider = UIView()
        addSubview(devider)
        devider.backgroundColor = #colorLiteral(red: 0.8862745098, green: 0.8941176471, blue: 0.937254902, alpha: 1)
        devider.setDimensions(height: 1, width: self.frame.width)
        devider.anchor(left: leftAnchor, bottom: bottomAnchor, right: rightAnchor)
        
        let leftLabel = UILabel()
        leftLabel.frame = CGRect(x: 0, y: 0, width: 27, height: 29)
        leftLabel.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        leftLabel.font = UIFont(name: "Roboto-Bold", size: 24)
        leftLabel.text = "+7"
        
        leftView = leftLabel
        leftView?.frame = CGRect(x: 0, y: 0, width: 29, height: 29)
        leftViewMode = .always
        
        borderStyle = .none
        textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        font = UIFont(name: "Roboto-Bold", size: 24)
        keyboardType = .phonePad
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit() {
        super.delegate = self
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.leftViewRect(forBounds: bounds)
        rect.origin.x += 12
        return rect
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 50, dy: 0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 50, dy: 0)
    }
    
    
}

extension PhoneNumberTextField: UITextFieldDelegate {
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return self.realDelegate?.textFieldShouldBeginEditing?(textField) ?? true
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        self.realDelegate?.textFieldDidBeginEditing?(textField)
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return self.realDelegate?.textFieldShouldEndEditing?(textField) ?? true
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.realDelegate?.textFieldDidEndEditing?(textField)
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
                
        let previousMask = self.stringMask
        let currentText: NSString = textField.text as NSString? ?? ""
        
        if let realDelegate = self.realDelegate, realDelegate.responds(to: #selector(textField(_:shouldChangeCharactersIn:replacementString:))) {
            let delegateResponse = realDelegate.textField!(textField, shouldChangeCharactersIn: range, replacementString: string)
            
            if !delegateResponse {
                return false
            }
        }
        
        guard let mask = self.stringMask else { return true }
        
        let newText = currentText.replacingCharacters(in: range, with: string)
        var formattedString = mask.mask(string: newText)
        
        if (previousMask != nil && mask != previousMask!) || formattedString == nil {
            let unmaskedString = mask.unmask(string: newText)
            formattedString = mask.mask(string: unmaskedString)
        }
        
        guard let finalText = formattedString as NSString? else { return false }
        
        if finalText == currentText && range.location < currentText.length && range.location > 0 {
            return self.textField(textField, shouldChangeCharactersIn: NSRange(location: range.location - 1, length: range.length + 1) , replacementString: string)
        }
        
        if finalText != currentText {
            textField.text = finalText as String
            
            if range.location < currentText.length {
                var cursorLocation = 0
                
                if range.location > finalText.length {
                    cursorLocation = finalText.length
                } else if currentText.length > finalText.length {
                    cursorLocation = range.location
                } else {
                    cursorLocation = range.location + 1
                }
                
                guard let startPosition = textField.position(from: textField.beginningOfDocument, offset: cursorLocation) else { return false }
                guard let endPosition = textField.position(from: startPosition, offset: 0) else { return false }
                textField.selectedTextRange = textField.textRange(from: startPosition, to: endPosition)
            }
            
            return false
        }
        
        return true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return self.realDelegate?.textFieldShouldClear?(textField) ?? true
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return self.realDelegate?.textFieldShouldReturn?(textField) ?? true
    }
    
}
